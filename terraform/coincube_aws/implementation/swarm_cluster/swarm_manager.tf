data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "swarm_manager" {
  source               = "../../../modules/aws_ec2_instance"
  name                 = "cc-swarm-manager"
  environment          = "${var.environment}"
  instance_type        = "${var.swarm_manager_instance_type}"
  ami                  = "${data.aws_ami.ubuntu.image_id}"
  key_name             = "${var.ec2_key_name}"
  security_groups      = "${data.terraform_remote_state.vpc.sg_internal_traffic}"
  subnet_id            = "${data.terraform_remote_state.vpc.private_subnets}"
  user_data            = "${file("../../scripts/userdata.sh")}"
  iam_instance_profile = "${aws_iam_instance_profile.swarm.name}"
}

# orchestration part
resource "null_resource" "ansible" {
  triggers {
    swarm_manager = "${module.swarm_manager.private_ip}"
  }

  connection {
    type        = "ssh"
    host        = "${module.swarm_manager.private_ip}"
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key)}"
  }

  provisioner "remote-exec" {
    script = "../../scripts/wait-for-instance.sh"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i ${module.swarm_manager.private_ip}, --user=${var.ssh_user} --private-key=${var.ssh_private_key} ${var.ansible_playbook}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo docker swarm init --advertise-addr ${module.swarm_manager.private_ip} 2>/dev/null",
      "sudo docker swarm join-token -q manager > /home/ubuntu/manager.token",
      "sudo docker swarm join-token -q worker > /home/ubuntu/worker.token",
      "sudo aws s3 cp /home/ubuntu/manager.token s3://${aws_s3_bucket.s3_bucket_swarm.bucket}/",
      "sudo aws s3 cp /home/ubuntu/worker.token s3://${aws_s3_bucket.s3_bucket_swarm.bucket}/",
    ]
  }

  depends_on = ["module.swarm_manager"]
}
