variable "target_group_arn" {
  description = "Target group ARN"
}

variable "port" {
  description = "The port on which targets receive traffic"
}

variable "instance_ips" {
  description = "EC2 Instance IP addresses"
  type        = "list"
}

variable "iterator" {
  description = "number of iterations"
}
