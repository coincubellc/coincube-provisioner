variable "region" {
  description = "AWS region"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "environment" {
  description = "environment name"
}

variable "redis_instance_type" {
  description = "type of instance"
}

variable "redis_node_num" {
  description = "number of nodes in the cluster"
}

variable "redis_data_route53_record" {
  description = "name of the dns record for redis instance"
}

variable "redis_brain_route53_record" {
  description = "name of the dns record for redis instance"
}

variable "redis_cron_job_route53_record" {
  description = "name of the dns record for redis instance"
}

variable "redis_sentry_route53_record" {
  description = "name of the dns record for redis instance"
}

variable "redis_sentry" {
  description = "Number of instances for sentry"
  default     = 0
}