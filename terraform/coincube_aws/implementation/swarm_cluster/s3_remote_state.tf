terraform {
  backend "s3" {
    key = "swarm_cluster/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${var.bucket}"
    key    = "vpc/terraform.tfstate"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "route53" {
  backend = "s3"

  config {
    bucket = "${var.bucket}"
    key    = "route53/terraform.tfstate"
    region = "${var.region}"
  }
}
