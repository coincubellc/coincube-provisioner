#!make

SHELL := /usr/bin/env bash
.DEFAULT_GOAL := help
.PHONY: clean doc get env init plan apply destroy

CONFIG_DIR := $(CURDIR)/../../conf/$(shell echo $(env) | tr [:upper:] [:lower:])

BACKEND_ARGS := -backend-config=$(CONFIG_DIR)/default.backend

VARFILE_ARGS := -var-file=$(CONFIG_DIR)/default.backend \
				-var-file=$(CONFIG_DIR)/default.tfvars \
              	-var-file=$(CONFIG_DIR)/$(shell basename $(CURDIR)).tfvars

help:
	@echo 'Usage:'
	@echo '  make help                                   show this information'
	@echo '  make clean                                  delete .terraform directory and terraform.tfstate.backup file'
	@echo '  make doc                                    generate README.md file of a folder'
	@echo '  make get                                    run terraform get for a folder'
	@echo '  make env=<EnvName> init                     run terraform init'
	@echo '  make env=<EnvName> args="<TFArgs>" plan     run terraform plan'
	@echo '  make env=<EnvName> args="<TFArgs>" apply    run terraform apply'
	@echo '  make env=<EnvName> args="<TFArgs>" destroy  run terraform destroy'

clean:
	rm -rf .terraform terraform.tfstate.backup

doc:
	terraform-docs md . > README.md

get:
	terraform get

init:
	terraform init $(BACKEND_ARGS) $(VARFILE_ARGS)

plan:
	terraform plan $(VARFILE_ARGS) $(args)

apply:
	terraform apply $(VARFILE_ARGS) $(args)

destroy:
	terraform destroy $(VARFILE_ARGS) $(args)