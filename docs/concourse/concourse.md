# How to setup vault with concourse

* Create a mount in value for use by Concourse pipelines

  ```bash
  vault mount -path=/concourse -description="Secrets for concourse pipelines" generic
  ```

  ​

* Create a policy file (e.g. [`policy.hcl`](https://github.com/rahul-kj/concourse-vault/blob/master/vault-policy.hcl)) with the following content:

  ```
  path "concourse/*" {
    policy = "read"
    capabilities =  ["read", "list"]
  }
  ```

* Register the policy above with Vault: 

  ```bash
  vault policy-write policy-concourse policy.hcl
  ```


* Enable approle auth method

  `vault auth enable approle`

* Create approle 

  ```bash
  vault write auth/approle/role/concourse \
  secret_id_ttl=10m \
  token_num_uses=10 \
  token_ttl=20m \
  token_max_ttl=30m  \
  secret_id_num_uses=40 \
  policies=policy-concourse
  ```

* Read role id and secret id

  ```bash
  :~$ vault read auth/approle/role/concourse/role-id
  Key        Value
  ---        -----
  role_id    e85e6303-4574-df94-fdc3-16b60f97eb56

  :~$ vault write -f auth/approle/role/concourse/secret-id
  Key                   Value
  ---                   -----
  secret_id             1b0086b1-4456-98c9-1636-045c2e7f5475
  secret_id_accessor    024c84f4-a6bf-3216-bedb-c1cd9b3235ab
  ```

  ​

[More info here](https://github.com/pivotalservices/concourse-pipeline-samples/tree/master/concourse-pipeline-patterns/vault-integration)

# How to create a token
`vault token create -display-name coincube -period 525600m -policy=coincube`

# How to write git private key into vault
`vault write concourse/main/gitlab value=@concourse-deploy-key.pem`
