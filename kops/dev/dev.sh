export KOPS_STATE_STORE=s3://coincube-kops-dev
export AWS_REGION=us-east-1
export CLUSTER_NAME=k8s.develop.coincube.io
export VPC_ID=vpc-32a5e04a # replace with your VPC id
export SUBNETS_IDS='subnet-80cdb0e4,subnet-426e1e09,subnet-e66616ad,subnet-c3d1aca7'
export NATGW_ID='nat-066d1f22cd1efcbd7'
export NETWORK_CIDR=10.0.0.0/16 # replace with the cidr for the VPC ${VPC_ID}
export DNS_ZONE=Z1R5R6LT6NE6PC
export MASTER_AZS='us-east-1a'
export NODE_AZS='us-east-1a,us-east-1c'
export SSHKEY_PATH='coincube-us-east1.pub'

# Nodes definitions
export MASTER_COUNT=1
export MASTER_SIZE='t2.medium'
export NODE_SIZE='t2.medium'
export NODE_COUNT=2
export CLOUD_LABELS="Environment=dev,Kops=true,kubernetes.io/cluster/$CLUSTER_NAME=owned"
export SEC_GROUPS='sg-506e3525'

# Terraform
TFSTATE_BUCKET='s3://coincube-terraform-dev'
