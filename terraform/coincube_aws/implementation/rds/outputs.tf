# DB subnet group
output "db_subnet_group_id" {
  description = "The db subnet group name"
  value       = "${module.db_subnet_group_main.db_subnet_group_id}"
}

# DB instance
output "db_instance_address" {
  description = "The address of the RDS instance"
  value       = "${module.cc_db_instance.db_instance_address}"
}

output "db_instance_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = "${module.cc_db_instance.db_instance_availability_zone}"
}

output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = "${module.cc_db_instance.db_instance_endpoint}"
}

output "db_instance_id" {
  description = "The RDS instance ID"
  value       = "${module.cc_db_instance.db_instance_id}"
}

output "db_name" {
  description = "The database name"
  value       = "${module.cc_db_instance.db_name}"
}

output "db_instance_username" {
  description = "The master username for the database"
  value       = "${module.cc_db_instance.db_instance_username}"
}

#output "db_instance_password" {
#  description = "The database password (this password may be old, because Terraform doesn't track it after initial creation)"
#  value       = "${var.password}"
#}

output "db_instance_port" {
  description = "The database port"
  value       = "${module.cc_db_instance.db_instance_port}"
}

output "cc_db_instance_fqdn" {
  value = "${aws_route53_record.cc_db_instance.fqdn}"
}
