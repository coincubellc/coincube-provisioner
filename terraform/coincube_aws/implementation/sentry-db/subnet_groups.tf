module "db_subnet_group_main" {
  source     = "../../../modules/database/rds/subnet-group"
  subnet_ids = ["${data.terraform_remote_state.vpc.private_subnets}"]
  name       = "cc-db-subnetgroup"
  env        = "${var.environment}"
}
