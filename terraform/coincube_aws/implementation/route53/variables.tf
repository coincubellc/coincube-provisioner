variable "region" {
  description = "AWS region"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "environment" {
  description = "environment name"
}

variable "route53_zone_name" {
  description = "name of the main coincube's zone to create"
}