#!/bin/bash
# Install python to run ansible
sudo apt-get update && sudo apt-get -y install python python-pip;
pip install awscli;
pip install ansible==2.3.2.0;
