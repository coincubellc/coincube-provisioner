output "swarm_manager_id" {
  value = "${module.swarm_manager.id}"
}

output "swarm_manager_public_ip" {
  value = "${module.swarm_manager.public_ip}"
}

output "swarm_manager_private_ip" {
  value = "${module.swarm_manager.private_ip}"
}

output "swarm_external_lb_fqdn" {
  value = "${aws_route53_record.swarm_external_lb.fqdn}"
}