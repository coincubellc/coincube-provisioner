# How to bring up services #

`$(aws ecr get-login --no-include-email --region us-east-1);`
`for image in c648212798771.dkr.ecr.us-east-1.amazonaws.com/pillar 648212798771.dkr.ecr.us-east-1.amazonaws.com/coincube 648212798771.dkr.ecr.us-east-1.amazonaws.com/nginx; do docker pull $image; done`
`docker stack deploy -c docker-compose.yml --with-registry-auth coincube;`

### Remember to have this ports open: ###
The following ports must be available. On some systems, these ports are open by default.

TCP port 2377 for cluster management communications
TCP and UDP port 7946 for communication among nodes
UDP port 4789 for overlay network traffic

# Deploying and updating the cluster #

## Deployments ##
### In jenkins: ###
When the job prod/build pillar runs automatically triggers two jobs: deploy_swarm_coincube and deploy_swarm_pillar
That will update the docker swarm cluster with the new images, if for some reason those jobs are not triggered, you can run them manually.

### Deploying manually: ###
In case that jenkins jobs don't work you can log into swarm master instance by ssh and run manually the following commands (root needed):
#### For pillar ####
```
docker login -u $user -p $pass;
docker pull coincube/pillar:prod;
docker service update --image=coincube/pillar:prod coincube_ordergen;
docker service update --image=coincube/pillar:prod coincube_raw2candle;
docker service update --image=coincube/pillar:prod coincube_gemgen;
docker service update --image=coincube/pillar:prod coincube_wavegen;
docker service update --image=coincube/pillar:prod coincube_krgen;
docker service update --image=coincube/pillar:prod coincube_datagen;
```

#### For flask ####
```
docker login -u $user -p $pass;
docker pull coincube/coincube:prod;
docker service update --image coincube/coincube:prod coincube_ccflask;
```

## Updating the cluster ##
In case that you update the docker swarm file coincube.yml and want to apply your changes you have to:
- ssh in swarm master
- copy coincube.yml file
- as root run `docker stack deploy -c coincube.yml --with-registry-auth coincube`

If you need to update nginx config files, they are located in swarm master in /srv/coincibe.io/etc after that run the stack deploy
