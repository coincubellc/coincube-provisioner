
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| count | Whether to create this resource or not? | string | `1` | no |
| env | Environment | string | - | yes |
| family | The family of the DB parameter group | string | - | yes |
| identifier | The identifier of the resource | string | - | yes |
| name_prefix | Creates a unique name beginning with the specified prefix | string | - | yes |
| parameters | A list of DB parameter maps to apply | string | `<list>` | no |

## Outputs

| Name | Description |
|------|-------------|
| this_db_parameter_group_arn | The ARN of the db parameter group |
| this_db_parameter_group_id | DB parameter group |

