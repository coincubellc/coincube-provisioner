# How to use #
```
terraform init --backend-config=../../conf/${environment}/default.backend
terraform plan -var-file=${path_to_repo}/coincube-provisioner/terraform/coincube_aws/conf/dev/default.backend -var-file=${path_to_repo}/coincube-provisioner/terraform/coincube_aws/conf/dev/dev.tfvars -var=ssh_private_key=${path_to_private_key}/coincube-us-east1.pem
terraform apply -var-file=${path_to_repo}/coincube-provisioner/terraform/coincube_aws/conf/dev/default.backend -var-file=${path_to_repo}/coincube-provisioner/terraform/coincube_aws/conf/dev/dev.tfvars -var=ssh_private_key=${path_to_private_key}/coincube-us-east1.pem
```

If after first apply, on next plan terraform wants to rebuild the launch configurations you have to add this to your plan and apply to update the userdata:
```
--target=data.template_file.asg_userdata_manager --target=data.template_file.asg_userdata_worker
```
After that terraform should not try to rebuild the launch configuration.