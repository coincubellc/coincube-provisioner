# Coincube.io dns migration

Once we are ready for prod, the only step to take is changing the coincube.io A record to point to the kubernetes external load balancer.

1. Go to AWS Route53 service and click on coincube.io zone, or go to [this link](https://console.aws.amazon.com/route53/home?region=us-east-1#resource-record-sets:ZECZERH3USB4J) 

    There is an A record coincube.io pointing to IP 138.197.240.80, that's the ip of the DigitalOcean's loadbalancer, (in case you need to rollback all of this, just create the record again with that IP).

2. Delete the A record

3. Connect to prod vpn and export the prod kubeconfig:

    ```bash
    $ export KUBECONFIG=~/.kube/config-prod
    ```

4. Go to helm repo and edit the file `conf/prd/coincube-front.yaml`:

    On line 24 replace:
   
    `dns.alpha.kubernetes.io/internal` with `dns.alpha.kubernetes.io/external`

    On line 25 replace:

    `zalando.org/aws-load-balancer-scheme: internal` with `zalando.org/aws-load-balancer-scheme: external`

    After that run a helm deploy:

    ```bash
    $ helm upgrade coincube-front -f conf/prod/coincube-front.yaml --set 'image.tag=master.IDOFTHECOMMITYOUWANTTODEPLOY' coincube-front/
    ```

    Commit your changes and push to the repo. You may need to wait a few mins until the loadbalancer is switched from internal (only accesible through vpn) to external (reacheable through internet)

5. In this step you need to obtain the loadbalancer dns name to replace in the coincube.io record

    ```bash
    $ kubectl get ing -owide | grep coincube-front
    ```

    It will output something like

    ```
    coincube-front            coincube.io,coincube-front.prod.coincube.io   kube-ing-lb-xfdmbqiw9fg3-233493947.us-east-1.elb.amazonaws.com   80        3d
    ```

    What matters for us is `kube-ing-lb-xfdmbqiw9fg3-233493947.us-east-1.elb.amazonaws.com` that's the aws record for the loadbalancer, at the time of this writing that's the url of the loadbalancer but it may change since it's dynamic that's the reason why you need to obtain it running `kubectl get ing -owide | grep coincube-front`

6. Go again to Route53 and create an A record `coincube.io` and click yes on the alias button and place the loadbalancer address on *Alias target*. Save it and that's it, you only need to wait until the dns cache is refreshed

