resource "aws_s3_bucket" "terraform_bucket_dev" {
  bucket        = "coincube-terraform-dev"
  acl           = "private"
  force_destroy = true

  tags {
    Name        = "coincube-terraform-dev"
    Environment = "dev"
  }
}

resource "aws_s3_bucket" "terraform_bucket_stg" {
  bucket        = "coincube-terraform-stg"
  acl           = "private"
  force_destroy = true

  tags {
    Name        = "coincube-terraform-stg"
    Environment = "stg"
  }
}

resource "aws_s3_bucket" "terraform_bucket_prd" {
  bucket        = "coincube-terraform-prd"
  acl           = "private"
  force_destroy = true

  tags {
    Name        = "coincube-terraform-prd"
    Environment = "prd"
  }
}
