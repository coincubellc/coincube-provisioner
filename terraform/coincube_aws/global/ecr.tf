resource "aws_ecr_repository" "cc_pillar" {
  name = "pillar"
}

resource "aws_ecr_repository" "cc_coincube" {
  name = "coincube"
}

resource "aws_ecr_repository" "cc_nginx" {
  name = "nginx"
}
