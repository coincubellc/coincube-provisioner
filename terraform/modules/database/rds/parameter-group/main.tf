resource "aws_db_parameter_group" "this" {
  count       = "${var.count}"
  name_prefix = "${var.name_prefix}"
  description = "Database parameter group for ${var.identifier}"
  family      = "${var.family}"
  parameter   = ["${var.parameters}"]

  tags = {
    Name        = "${lower(var.name_prefix)}.${lower(var.env)}"
    Environment = "${var.env}"
    Creator     = "terraform"
  }
}
