# coincube-provisioner

## Terraform ##
Terraform is an opensource tool for orchestration and infrastructure especification, in this case we use terraform to create and provision the instances in digitalocean with ansible.
Terraform has a big community and is well documented, also have support for multiple cloud services.

For more info about terraform and documentation: https://www.terraform.io/docs/index.html

How to use the terraform code in this repository:

`cd terraform`

Here you have to create a file with your Digital Ocean API token, in this case i created a file called gnasr-do-token (it's not commited in the repo)

`cd docker-swarm`

Here you have multiple files but the most important are this:
 - variables.tf: This file contains the variables definitions that are used by terraform (one of it's most powerfull features) this file may contain default values but if not you have 2 ways to set the variables (this is all extended on terraform documentation):
    - Setting the variables on runtime `terraform apply --var=variable=value`
    - Using an external variable file `terraform apply --var-file=variables.tfvars`
 - main.tf: This is the main terraform file that contains all the code.

Once you have set all the variables required by terraform you can see what terraform wants to do (without applying changes)
`terraform plan` or depending on the way you are setting the variables:
    - `terraform plan --var-file=variables.tfvars`
    - `terraform plan --var=do_region="nyc2" --var=ssh_private_key="/home/ubuntu/.ssh/id_rsa"`

This will output a list of all the resources terraform wants to create or modify (if already exist and the code has changed)

If you are confident with the plan output you can apply changes:
`terraform apply --var-file=variables.tfvars`

This will create a file called terraform.tfstate (it's important to always commit this file because contains the ids and definitions used by terraform in order to not create again all the resources on every run)

This code has defined ouputs like the digitalocean instances IDs, private/public ips and loadbalancer ID/ip 

A more advance use of terraform is using modules (like the code in this repo) this enables you to write more complex code and reutilize it like libraries, the modules are located in the folder called "modules".

## On a high level this terraform code will do ##

1. Create a docker swarm manager node that will be provisioned with ansible running the common role and a docker installation role, then will start the docker swarm cluster and store the manager and node tokens in two files for later use
2. Create swarm worker nodes (you can define the quantity on a variable named count in main.tf) if you want to scale the cluster you only have to change the count to a bigger or lower number and run terraform. This instances are provisioned like the swarm mannager node but the difference is that this module joins the cluster by using the docker swarm token stored in the file created by master.
3. Create a load balancer with the ids of the swarm manager and worker instances.