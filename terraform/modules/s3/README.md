
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| env | Environment name (e.g. Ops, Dev, Int, Stage, Preview, Prod) | string | - | yes |
| name_infix | S3 bucket name prefix | string | - | yes |
| policy | S3 bucket policy in json format | string | `` | no |
| region | AWS region name (e.g. us-east-1, us-west-2, etc) | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn |  |
| bucket |  |

