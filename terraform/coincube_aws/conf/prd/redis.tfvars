# redis elasticache
redis_instance_type           = "cache.t2.medium"
redis_node_num                = "1"
redis_data_route53_record     = "redis-data"
redis_brain_route53_record    = "redis-brain"
redis_sentry_route53_record   = "redis-sentry"
redis_cron_job_route53_record = "redis-cron-job"
redis_sentry                  = 1