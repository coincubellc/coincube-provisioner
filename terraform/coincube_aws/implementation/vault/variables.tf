variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "region" {}
variable "environment" {}
variable "ec2_key_name" {}
variable "vault_route53_record" {}
variable "vault_instance_route53_record" {}
variable "vault_instance_type" {}
variable "ami" {}
variable "cert_arn" {}