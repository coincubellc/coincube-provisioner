variable "count" {
  description = "Whether to create this resource or not?"
  default     = 1
}

variable "name" {
  description = "The identifier of the resource"
}

variable "env" {
  description = "Environment"
}

variable "subnet_ids" {
  type        = "list"
  description = "A list of VPC subnet IDs"
  default     = []
}
