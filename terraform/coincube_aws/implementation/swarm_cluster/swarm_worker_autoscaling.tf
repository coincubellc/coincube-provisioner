data "template_file" "asg_userdata_worker" {
  template = "${file("../../scripts/userdata_asg.sh")}"

  vars {
    token      = "worker"
    manager_ip = "${module.swarm_manager.private_ip}"
  }

  depends_on = ["null_resource.ansible"]
}

# swarm workers lc
module "swarm_worker_lc" {
  count                = "1"
  source               = "../../../modules/autoscaling/launch_configuration"
  name                 = "cc-swarm-worker-lc-${var.environment}"
  image_id             = "${data.aws_ami.ubuntu.image_id}"
  instance_type        = "${var.swarm_worker_instance_type}"
  key_name             = "${var.ec2_key_name}"
  security_groups      = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
  iam_instance_profile = "${aws_iam_instance_profile.swarm.name}"
  user_data            = "${data.template_file.asg_userdata_worker.rendered}"

  root_block_device = [
    {
      volume_size = "15"
      volume_type = "gp2"
    },
  ]
}

module "swarm_worker_autoscaling_group" {
  source                    = "../../../modules/autoscaling/autoscaling_group"
  name                      = "cc-swarm-worker-asg-${var.environment}"
  launch_configuration      = "${module.swarm_worker_lc.launch_configuration_id}"
  vpc_zone_identifier       = ["${data.terraform_remote_state.vpc.private_subnets}"]
  health_check_type         = "EC2"
  min_size                  = "${var.swarm_worker_asg_min_size}"
  max_size                  = "${var.swarm_worker_asg_max_size}"
  desired_capacity          = "${var.swarm_worker_asg_desired_capacity}"
  wait_for_capacity_timeout = "${var.swarm_worker_asg_wait_for_capacity_timeout}"
  target_group_arns         = ["${module.swarm_external_lb.target_group_id}"]

  tags = [
    {
      key                 = "Environment"
      value               = "${var.environment}"
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "asg-swarm-worker-${var.environment}"
      propagate_at_launch = true
    },
  ]
}
