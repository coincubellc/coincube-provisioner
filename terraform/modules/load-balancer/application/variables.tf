variable "internal" {
  description = "Is internal load balancer. Internal: true, Internet facing: false"
  default     = true                                                                # false for internet facing
}

variable "idle_timeout" {
  description = "Idle timeout"
  default     = 60
}

variable "ssl_policy" {
  description = "SSL policy text. Required for any listener using HTTPS protocol."
  default     = ""
}

variable "certificate_arn" {
  description = "SSL certificate ARN.  Required for any listener using HTTPS protocol."
  default     = ""
}

variable "enable_deletion_protection" {
  description = "Deletion protection enabling. Enabled: true, Disabled: false"
  default     = false
}

variable "vpc_id" {
  description = "VPC Id"
}

variable "env" {
  description = "Environment name (e.g. Ops, Dev, Int, Stage, Preview, Prod)"
}

variable "name" {
  description = "Load balancer name"
}

variable "subnet_ids" {
  description = "List of subnet ids"
  type        = "list"
}

variable "sg_ids" {
  description = "List of security group ids"
  type        = "list"
}

# Ex: listener = {
#   "0" = [ "HTTP", "80" ]  # protocol (HTTP, HTTPS), port
#   "1" = [ "HTTPS", "443" ]
#}
variable "listeners" {
  description = "Map of lists of listener protocol -> port"
  type        = "map"

  default = {
    "0" = ["HTTP", "80"]
  }
}

variable "target_group_protocol" {
  description = "Target group protocol (HTTP or HTTPS)"
  default     = "HTTP"
}

variable "target_group_port" {
  description = "Target group port"
  default     = "80"
}

variable "target_type" {
  description = "The type of target that you must specify when registering targets with this target group. The possible values are instance (targets are specified by instance ID) or ip (targets are specified by IP address). The default is instance. Note that you can't specify targets for a target group using both instance IDs and IP addresses. If the target type is ip, specify IP addresses from the subnets of the virtual private cloud (VPC) for the target group, the RFC 1918 range (10.0.0.0/8, 172.16.0.0/12, and 192.168.0.0/16), and the RFC 6598 range (100.64.0.0/10). You can't specify publicly routable IP addresses."
  default     = "instance"
}

variable "health_check_protocol" {
  description = "Health check protocol"
  default     = "HTTP"
}

variable "health_check_path" {
  description = "Health check path"
  default     = "/"
}

variable "health_check_healthy_threshold" {
  description = "Health check healty threshold"
  default     = 5
}

variable "health_check_unhealthy_threshold" {
  description = "Health check unhealthy threshold"
  default     = 2
}

variable "health_check_timeout" {
  description = "Health check timeout"
  default     = 5
}

variable "health_check_interval" {
  description = "Health check internal"
  default     = 30
}

variable "health_check_success_codes" {
  description = "Coma separated HTTP codes to use when checking for a successful response from a target"
  default = "200"
}
