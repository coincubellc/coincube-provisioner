vpc_name_prefix       = "cc"
vpc_azs               = ["us-east-1a", "us-east-1b", "us-east-1c"]
vpc_cidr              = "10.1.0.0/16"
private_subnets       = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]
public_subnets        = ["10.1.11.0/24", "10.1.12.0/24", "10.1.13.0/24"]
single_nat_gateway    = false
openvpn_instance_type = "t2.small"
key_name              = "coincube-us-east1"