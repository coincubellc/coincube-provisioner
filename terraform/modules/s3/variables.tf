variable "env" {
  description = "Environment name (e.g. Ops, Dev, Int, Stage, Preview, Prod)"
}

variable "region" {
  description = "AWS region name (e.g. us-east-1, us-west-2, etc)"
}

variable "policy" {
  description = "S3 bucket policy in json format"
  default     = ""
}

variable "name_infix" {
  description = "S3 bucket name prefix"
}

# Ex:
# variable "topic_notifications" {
#   description = "Topic notification configuration"
#   type = "list"
#   default = [
#     {
#        id = "Unique id"
#        topic_arn = "topic_arn"
#        events = ["s3:ObjectCreated:Put"]
#     }
#   ]
# }
variable "topic_notifications" {
  description = "Topic notification configuration list"
  type        = "list"
  default     = []
}

# Ex:
# variable "queue_notifications" {
#   description = "Queue notification configuration"
#   type = "list"
#   default = [
#     {
#        id = "Unique id"
#        queue_arn = "queue_arn"
#        events = ["s3:ObjectCreated:Put"]
#     }
#   ]
# }
variable "queue_notifications" {
  description = "Queue notification configuration list"
  type        = "list"
  default     = []
}

variable "name" {}