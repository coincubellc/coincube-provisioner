variable "region" {
  description = "AWS region"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "environment" {
  description = "environment name"
}

variable "allocated_storage" {
  description = "Size of the db storage"
}

variable "instance_class" {
  description = "type of instane"
}

variable "db_name" {
  description = "name of the database to be created"
}

variable "username" {
  description = "administrator username"
}

variable "password" {
  description = "administrator password"
}

variable "db_route53_record" {
  description = "name of the dns record for database instance"
}
