##################
# DB subnet group
##################
resource "aws_db_subnet_group" "this" {
  count = "${var.count}"

  name_prefix = "${var.name}"
  description = "Database subnet group for ${var.name}"
  subnet_ids  = ["${var.subnet_ids}"]

  tags = {
    Name        = "db-subnetgroup-${var.name}"
    Environment = "${var.env}"
    Creator     = "terraform"
  }
}
