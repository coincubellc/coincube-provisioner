# How to setup vault
## High level explaination:
This implementation creates:
* 1 vault server instance
* load balancer for vault api
* route53 record for the loadbalancer

## How to use this
* First you have to deploy everything using terraform

```
make init env=${env}
make plan env=${env}
make apply env=${env}
```

Onces everything was installed by ansible you have to init vault and unseal the vault server with the keys
It's recomended to setup 5 keys with a treshold of 3

### How to init and unseal:
```
ssh ubuntu@$VAULT_INSTANCE_IP
curl \
    --request POST \
    --data '{"secret_shares": 5, "secret_threshold": 3}' \
    http://localhost:8200/v1/sys/init
```

This will output 5 keys and the root token, store everything in a secure place
Now unseal the vault server
```
export VAULT_ADDR=http://localhost:8200
vault operator unseal <-- this step will ask for a key you have to use 3 keys and run this step 3 times to unseal the server
```

Now that you the vault server unealed you can check if it's working by doing a curl to the endpoint `v1/sys/health` and start using the loadbalancer url for vault api
