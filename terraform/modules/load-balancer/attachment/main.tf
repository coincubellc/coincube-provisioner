resource "aws_lb_target_group_attachment" "attachment" {
  count            = "${var.iterator}"
  target_group_arn = "${var.target_group_arn}"
  target_id        = "${element(var.instance_ips, count.index)}"
  port             = "${var.port}"
}
