resource "aws_s3_bucket" "s3_bucket_swarm" {
  bucket        = "cc-s3-bucket-swarm-${var.environment}"
  acl           = "private"
  force_destroy = true

  tags {
    Name        = "cc-s3-bucket-swarm-${var.environment}"
    Environment = "${var.environment}"
  }
}
