variable "region" {
  description = "AWS region"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "environment" {
  description = "environment name"
}

variable "volume_size" {
  description = "Size of the storage"
}

variable "instance_type" {
  description = "type of instance"
}

variable "instance_count" {
  description = "number of instances"
  default     = 1
}

variable "es_route53_record" {
  description = "name of the dns record for es endpoint"
}

variable "kibana_route53_record" {
  description = "name of the dns record for kibana endpoint"
}

variable "elasticsearch_version" {
  description = "version of ES"
  default     = "6.2"
}

variable "allow_ips" {
  description = "whitelist of ips to access the cluster"
}
