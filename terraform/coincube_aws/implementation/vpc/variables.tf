variable "region" {
  description = "AWS region"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "environment" {
  description = "environment name"
}

variable "vpc_name_prefix" {
  description = "name of the vpc"
}

variable "vpc_cidr" {
  description = "cidr of the vpc"
}

variable "vpc_azs" {
  description = "list of availability zones for the vpc (must match subnets cidr count)"
  type        = "list"
}

variable "private_subnets" {
  description = "cidr list of private subnets"
  type        = "list"
}

variable "public_subnets" {
  description = "cird list of public subnets"
  type        = "list"
}

variable "key_name" {
  description = "name of the ec2 ssh key"
}

variable "openvpn_instance_type" {
  description = "type of ec2 instance"
}

variable "single_nat_gateway" {
  description = "Only one nat gateway for all the subnets"
  default     = true
}