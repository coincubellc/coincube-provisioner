export KOPS_STATE_STORE=s3://coincube-kops-prd
export AWS_REGION=us-east-1
export CLUSTER_NAME=k8s.prod.coincube.io
export VPC_ID=vpc-0774697c # replace with your VPC id
export SUBNETS_IDS='subnet-f9518dd7,subnet-ed3e5fa7,subnet-625f834c,subnet-83d705e4,subnet-9e3554d4,subnet-78c1131f'
export NETWORK_CIDR=10.1.0.0/16 # replace with the cidr for the VPC ${VPC_ID}
export DNS_ZONE=Z3R6ETUKGVXREB
export MASTER_AZS='us-east-1a,us-east-1b,us-east-1c'
export NODE_AZS='us-east-1a,us-east-1b,us-east-1c'
export SSHKEY_PATH='coincube-us-east1.pub'

# Nodes definitions
export MASTER_COUNT=3
export MASTER_SIZE='t2.large'
export NODE_SIZE='m4.large'
export NODE_COUNT=2
export CLOUD_LABELS="Environment=prod,Kops=true,kubernetes.io/cluster/$CLUSTER_NAME=owned"
export SEC_GROUPS='sg-5fa54f14'

# Terraform
TFSTATE_BUCKET='s3://coincube-terraform-prd'