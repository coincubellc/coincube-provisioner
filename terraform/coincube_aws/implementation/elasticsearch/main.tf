provider "aws" {
  region = "${var.region}"
}

resource "aws_elasticsearch_domain" "es" {
  domain_name           = "${var.environment}-logging"
  elasticsearch_version = "${var.elasticsearch_version}"

  cluster_config {
    instance_type  = "${var.instance_type}"
    instance_count = "${var.instance_count}"
  }

  ebs_options {
    ebs_enabled = true
    volume_type = "gp2"
    volume_size = "${var.volume_size}"
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  vpc_options {
    security_group_ids = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
    subnet_ids         = ["${element(data.terraform_remote_state.vpc.private_subnets, 0)}"]
  }

  access_policies = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:${var.region}:*"
    }
  ]
}
CONFIG

  tags {
    Environment = "${var.environment}"
  }
}

resource "aws_route53_record" "cc_elasticsearch" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.es_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_elasticsearch_domain.es.endpoint}"]
}

resource "aws_route53_record" "cc_kibana" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.kibana_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${element(split("/", aws_elasticsearch_domain.es.kibana_endpoint), 0)}"]
}
