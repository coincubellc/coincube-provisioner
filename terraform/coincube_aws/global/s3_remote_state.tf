terraform {
  backend "s3" {
    bucket = "coincube-terraform"
    key    = "global/terraform.tfstate"
    region = "us-east-1"
  }
}
