resource "digitalocean_droplet" "droplet" {
  image              = "${var.image}"
  name               = "${var.name}-${count.index}"
  region             = "${var.region}"
  size               = "${var.size}"
  private_networking = "${var.private_networking}"
  ssh_keys           = "${var.ssh_keys}"
  user_data          = "${var.user_data}"
  backups            = "${var.backups}"
  volume_ids         = "${var.volume_ids}"
  count              = "${var.count}"

  connection {
    type        = "ssh"
    host        = "${self.ipv4_address}"
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key)}"
  }

  provisioner "remote-exec" {
    script = "wait-for-instance.sh"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i ${self.ipv4_address}, --user=${var.ssh_user} --private-key=${var.ssh_private_key} ${var.ansible_playbook}"
  }

  provisioner "remote-exec" {
    inline = ["${var.token_file}"]
  }
}
