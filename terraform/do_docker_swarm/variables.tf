variable "do_token" {
  description = "Path to the file with the digitalocean api token"
  default     = "../gnasr-do-token"
}

variable "do_region" {
  description = "Region to use in digital ocean"
  default     = "nyc2"
}

variable "do_ssh_keys" {
  description = "A list of digital ocean's ssh keys"
  type        = "list"
  default     = ["d2:3b:f4:50:e7:3c:f9:ab:34:a8:53:61:71:54:f5:c6"]
}

variable "ssh_private_key" {
  description = "Path to the private ssh key to log into the instances"
  default     = "/Users/gabnasr/devops/coincube/gnasr.pem"
}
