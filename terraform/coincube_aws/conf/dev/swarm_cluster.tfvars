# swarm
swarm_manager_instance_type                 = "t2.small"
swarm_manager_asg_min_size                  = 0
swarm_manager_asg_max_size                  = 0
swarm_manager_asg_desired_capacity          = 0
swarm_manager_asg_wait_for_capacity_timeout = 0

swarm_worker_instance_type                 = "t2.small"
swarm_worker_asg_min_size                  = 0
swarm_worker_asg_max_size                  = 2
swarm_worker_asg_desired_capacity          = 2
swarm_worker_asg_wait_for_capacity_timeout = 0

# loadbalancer
lb_certificate_arn = "arn:aws:acm:us-east-1:648212798771:certificate/e6092324-1ab9-4cb4-84dc-332a3977572d"
lb_internal        = true

# route53
swarm_route53_record = "coincube"