
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| certificate_arn | SSL certificate ARN.  Required for any listener using HTTPS protocol. | string | `` | no |
| elastic_lb_account_id | Elastic load balancer account id (e.g. us-east-1: 127311923021, us-west-2: 797873946194) | string | - | yes |
| enable_deletion_protection | Deletion protection enabling. Enabled: true, Disabled: false | string | `false` | no |
| env | Environment name (e.g. Ops, Dev, Int, Stage, Preview, Prod) | string | - | yes |
| health_check_healthy_threshold | Health check healty threshold | string | `3` | no |
| health_check_interval | Health check internal | string | `30` | no |
| health_check_path | Health check path | string | `/` | no |
| health_check_protocol | Health check protocol | string | `HTTP` | no |
| health_check_success_codes | Coma separated health check success codes | string | `200` | no |
| health_check_timeout | Health check timeout | string | `10` | no |
| health_check_unhealthy_threshold | Health check unhealthy threshold | string | `3` | no |
| idle_timeout | Idle timeout | string | `60` | no |
| internal | Is internal load balancer. Internal: true, Internet facing: false | string | `true` | no |
| listeners | Map of lists of listener protocol -> port | map | `<map>` | no |
| name | Load balancer name | string | - | yes |
| region | AWS region name (e.g. us-east-1, us-west-2, etc) | string | - | yes |
| sg_ids | List of security group ids | list | `<list>` | no |
| ssl_policy | SSL policy text. Required for any listener using HTTPS protocol. | string | `` | no |
| subnet_ids | List of subnet ids | list | - | yes |
| target_group_port | Target group port | string | `80` | no |
| target_group_protocol | Target group protocol (HTTP or HTTPS) | string | `HTTP` | no |
| vpc_id | VPC Id | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn |  |
| target_group_id |  |