provider "aws" {
  region = "${var.region}"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "gitlab" {
  source                        = "../../../modules/aws_ec2_instance"
  name                          = "cc-gitlab"
  environment                   = "${var.environment}"
  instance_type                 = "${var.instance_type}"
  ami                           = "${data.aws_ami.ubuntu.image_id}"
  key_name                      = "${var.ec2_key_name}"
  security_groups               = "${data.terraform_remote_state.vpc.sg_internal_traffic}"
  subnet_id                     = "${data.terraform_remote_state.vpc.private_subnets}"
  user_data                     = ""
  root_block_device_volume_size = "${var.root_block_device_volume_size}"
}

resource "aws_route53_record" "gitlab" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.gitlab_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.gitlab.private_ip}"]
}
