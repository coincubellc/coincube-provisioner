# Hashicorp's vault

Vault lives on an EC2 instance, and the frontend/ui lives on kubernetes.

In order to use vault you need to init it and unseal, [more info](https://www.vaultproject.io/intro/getting-started/deploy.html)

## Unseal Vault

### How to init and unseal manually:

1. Ssh into vault instance

   ```bash
   ssh -i coincube-us-east1.pem ubuntu@vault-server.prod.coincube.io
   ```

2. Do a vault init

   It's recomended to setup 5 keys with a treshold of 3

   ```bash
   $ curl \
       --request POST \
       --data '{"secret_shares": 5, "secret_threshold": 3}' \
       http://localhost:8200/v1/sys/init
   ```

   This will output 5 keys and the root token, store everything in a secure place

3. Now unseal the vault (this step is required everytime vault instance restarts or the service go down)

   ```bash
   $ export VAULT_ADDR=http://localhost:8200
   $ vault operator unseal <-- this step will ask for a key you have to use 3 keys and run this step 3 times to unseal the server
   ```

   

## Create user with limited permissions

For coincube-back service we read the secret key from vault, so coincube-back needs an user with readonly permissions on that key, the steps to create the user (token) are:

1. Export VAULT env variables in order to use it

   ```bash
   export VAULT_ADDR=https://vault.prod.coincube.io:8200
   export VAULT_TOKEN=<ROOT_TOKEN>
   ```

2. Create a policy file `coincube-policy.hcl`:

     ```
     path "secret/coincube" {
       policy = "read"
       capabilities =  ["read", "list"]
     }
     ```

3. Write the policy:

   ```bash
   vault policy-write coincube-policy coincube-policy.hcl
   ```

4. Create the token, the period can be whatever you want, just remember to create a new token after the period expires

   ```bash
   vault token create -display-name coincube -period 525600m -policy=coincube
   ```

## Add the token to kubernetes secret

If you check the file `helm/coincube-back/templates/_vars.tpl` there is something like

```yaml
  {
    name: VAULT_TOKEN,
    valueFrom: {
      secretKeyRef: { name: coincube-secrets, key: vaultToken}
    }
  } 
```

That means the value of that ENV variable is read from a kubernetes secret called coincube-secrets. 

For security reasons the file that defines that secret is not store in our repositories, it's placed on keybase in the path `/keybase/team/coincube.admin/prod/kubernetes/prod_k8s_secrets.yml`. 

For prod you need to create that secret, the way to do this is:

1. Copy the file to your computer:

   ```bash
   cp /keybase/team/coincube.admin/prod/kubernetes/prod_k8s_secrets.yml /tmp/
   ```

2. Encode the token in base64

   ```bash
   $ echo '89126608-ca50-80ed-369c-3f701ba084a1' | base64 
    ODkxMjY2MDgtY2E1MC04MGVkLTM2OWMtM2Y3MDFiYTA4NGExCg==
   ```

3. Edit the file with the new token

   ```yaml
   # Secrets are encoded in base64
   # To apply this file: kubectl apply -f $filename.yml
   # e.g: kubectl apply -f dev_k8s_secrets.yml
   ---
   apiVersion: v1
   kind: Secret
   metadata:
     name: coincube-secrets
     namespace: default
     labels:
       app: coincube
   data:
   # Set the encoded token on the variable
     vaultToken: ODkxMjY2MDgtY2E1MC04MGVkLTM2OWMtM2Y3MDFiYTA4NGExCg==
   ```

4. Apply the secret in kubernetes (you have to do this everytime the secret changes, or you modify the file). Must export `config-prod` for use by kubectl.
   ```bash
   $ export KUBECONFIG=~/.kube/config-prod
   $ kubectl apply -f /tmp/prod_k8s_secrets.yml
   secret "coincube-secrets" configured
   ```

If you want to add more secrets to that file just create a new variable below vaultToken with it's encoded value, and replicate what is on the _vars.tpl file with the name of the new variable