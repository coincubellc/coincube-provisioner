provider "aws" {
  region = "${var.region}"
}

module "cc_db_instance" {
  source                 = "../../../modules/database/rds/instance"
  name_prefix            = "cc-sentry-${var.environment}"
  env                    = "${var.environment}"
  allocated_storage      = "${var.allocated_storage}"
  engine                 = "postgres"
  engine_version         = "9.6.5"
  license_model          = "postgresql-license"
  port                   = "5432"
  instance_class         = "${var.instance_class}"
  db_name                = "${var.db_name}"
  username               = "${var.username}"
  password               = "${var.password}"
  maintenance_window     = "Mon:00:00-Mon:03:00"
  backup_window          = "09:46-10:16"
  parameter_group_name   = "default.postgres9.6"
  db_subnet_group_name   = "${data.terraform_remote_state.rds.db_subnet_group_id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "cc_db_instance" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.db_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.cc_db_instance.db_instance_address}"]
}
