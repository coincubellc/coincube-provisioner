provider "aws" {
  region = "${var.region}"
}

resource "aws_route53_zone" "coincube" {
  name = "${var.route53_zone_name}"
}

output "coincube_zone_id" {
  value = "${aws_route53_zone.coincube.zone_id}"
}