provider "digitalocean" {
  token = "${file(var.do_token)}"
}

module "swarm_managers" {
  source             = "../modules/do_ansible_swarm_manager"
  image              = "ubuntu-16-04-x64"
  name               = "swarm-manager"
  region             = "${var.do_region}"
  size               = "2gb"
  private_networking = true
  ssh_keys           = "${var.do_ssh_keys}"
  user_data          = "${file("userdata.sh")}"
  count              = "1"
  ssh_user           = "root"
  ssh_private_key    = "${var.ssh_private_key}"
  ansible_playbook   = "../ansible/playbooks/swarm_instances.yml"
}

data "template_file" "swarm_join" {
  template = "${file("swarm_join.sh")}"

  vars {
    token        = "${file("worker.token")}"
    manager_ip   = "${element(module.swarm_managers.droplet_private_ip, 0)}"
    manager_port = 2377
  }
}

module "swarm_workers" {
  source             = "../modules/do_ansible_swarm_worker"
  image              = "ubuntu-16-04-x64"
  name               = "swarm-worker"
  region             = "${var.do_region}"
  size               = "4gb"
  private_networking = true
  ssh_keys           = "${var.do_ssh_keys}"
  user_data          = "${file("userdata.sh")}"
  count              = "1"
  ssh_user           = "root"
  ssh_private_key    = "${var.ssh_private_key}"
  ansible_playbook   = "../ansible/playbooks/swarm_instances.yml"
  token_file         = "${data.template_file.swarm_join.rendered}"
}

# Setup load balancer
resource "digitalocean_loadbalancer" "public" {
  name                   = "coincube-frontend-lb"
  region                 = "nyc2"
  redirect_http_to_https = "true"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  forwarding_rule {
    tls_passthrough = "true"
    entry_port      = 443
    entry_protocol  = "https"

    target_port     = 443
    target_protocol = "https"
  }

  healthcheck {
    port     = 80
    protocol = "tcp"
  }

  droplet_ids = ["${element(module.swarm_managers.droplet_id, 0)}", "${element(module.swarm_workers.droplet_id, 0)}"]
}

# Outputs
output "managers_private_ips" {
  value = "${module.swarm_managers.droplet_private_ip}"
}

output "managers_public_ips" {
  value = "${module.swarm_managers.droplet_ip}"
}

output "managers_droplet_ids" {
  value = "${module.swarm_managers.droplet_id}"
}

output "workers_private_ips" {
  value = "${module.swarm_workers.droplet_private_ip}"
}

output "workers_public_ips" {
  value = "${module.swarm_workers.droplet_ip}"
}

output "workers_droplet_ids" {
  value = "${module.swarm_workers.droplet_id}"
}

output "loadbalancer_ip" {
  value = "${digitalocean_loadbalancer.public.ip}"
}

output "loadbalancer_id" {
  value = "${digitalocean_loadbalancer.public.id}"
}
