terraform {
  backend "s3" {
    key    = "route53/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${var.bucket}"
    key    = "rds/terraform.tfstate"
    region = "${var.region}"
  }
}
