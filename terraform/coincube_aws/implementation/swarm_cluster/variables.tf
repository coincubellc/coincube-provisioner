variable "ssh_user" {
  default = "ubuntu"
}

variable "ssh_private_key" {}

variable "ansible_playbook" {
  default = "../../../../ansible/playbooks/swarm_instances.yml"
}

variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "region" {}

variable "environment" {}
variable "ec2_key_name" {}

variable "swarm_manager_instance_type" {}
variable "swarm_manager_asg_min_size" {}
variable "swarm_manager_asg_max_size" {}
variable "swarm_manager_asg_desired_capacity" {}
variable "swarm_manager_asg_wait_for_capacity_timeout" {}

variable "swarm_worker_instance_type" {}
variable "swarm_worker_asg_min_size" {}
variable "swarm_worker_asg_max_size" {}
variable "swarm_worker_asg_desired_capacity" {}
variable "swarm_worker_asg_wait_for_capacity_timeout" {}

variable "lb_certificate_arn" {
  description = "SSL certificate ARN.  Required for any listener using HTTPS protocol"
}

variable "lb_internal" {
  description = "Is internal load balancer. Internal: true, Internet facing: false"
}

variable "swarm_route53_record" {
  description = "name of the dns record for the external loadbalancer"
}
