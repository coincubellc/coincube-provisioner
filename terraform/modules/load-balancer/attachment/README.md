
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| instance_ips | EC2 Instance IP addresses | list | - | yes |
| iterator | number of iterations | string | - | yes |
| port | The port on which targets receive traffic | string | - | yes |
| region | AWS region name (e.g. us-east-1, us-west-2, etc) | string | - | yes |
| target_group_arn | Target group ARN | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| target_group_attachment_id |  |

