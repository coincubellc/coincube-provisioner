## Create cluster using kops yaml format ##
Export env variables e.g:
```
source dev.sh
```

Run kops dry run to generate config file
```
kops create cluster \
--cloud=aws \
--kubernetes-version=1.8.4 \
--zones=${NODE_AZS} \
--ssh-public-key=${SSHKEY_PATH} \
--master-zones=${MASTER_AZS} \
--name=${CLUSTER_NAME} \
--vpc=${VPC_ID} \
--networking=weave \
--topology=private \
--api-loadbalancer-type=internal \
--dns-zone=${DNS_ZONE} \
--dns=public \
--node-count=${NODE_COUNT} \
--node-size=${NODE_SIZE} \
--master-count=${MASTER_COUNT} \
--master-size=${MASTER_SIZE} \
--node-security-groups=${SEC_GROUPS} \
--master-security-groups=${SEC_GROUPS} \
--cloud-labels=${CLOUD_LABELS} \
--dry-run \
-o yaml
```

Add policies to yaml file in order to use [kube-ingress-aws-controller](https://github.com/kubernetes/kops/tree/master/addons/kube-ingress-aws-controller)
```
  additionalPolicies:
    node: |
      [
        {
          "Effect": "Allow",
          "Action": [
            "acm:ListCertificates",
            "acm:DescribeCertificate",
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:AttachLoadBalancers",
            "autoscaling:DetachLoadBalancers",
            "autoscaling:DetachLoadBalancerTargetGroups",
            "autoscaling:AttachLoadBalancerTargetGroups",
            "autoscaling:DescribeLoadBalancerTargetGroups",
            "cloudformation:*",
            "elasticloadbalancing:*",
            "elasticloadbalancingv2:*",
            "ec2:DescribeInstances",
            "ec2:DescribeSubnets",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeRouteTables",
            "ec2:DescribeVpcs",
            "iam:GetServerCertificate",
            "iam:ListServerCertificates"
          ],
          "Resource": ["*"]
        }
      ]
```    

Edit subnets and nat gateway in yaml file as described [here](https://github.com/kubernetes/kops/blob/release-1.8/docs/run_in_existing_vpc.md), you must use, vpc, subnets and nat gateway ids from your current environment, you can get those values from terraform outputs or aws console
Create cluster using the modified yaml file
```
kops create -f kops.yml
kops create secret --name k8s.develop.coincube.io sshpublickey admin -i coincube-us-east1.pub
kops update cluster k8s.develop.coincube.io --yes
kops validate cluster
kubectl get nodes --show-labels
```

Configure dashboard
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl proxy&
```
You can access dashboard here http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

## Configure ingress controller (to expose services)
Create loadbalancer using awscli for aws ingress controller
```
aws ec2 --region $AWS_REGION create-security-group --vpc-id $VPC_ID --description ingress.$CLUSTER_NAME --group-name ingress.$CLUSTER_NAME 
aws ec2 describe-security-groups --filters Name=vpc-id,Values=$VPC_ID Name=group-name,Values=ingress.$CLUSTER_NAME --region $AWS_REGION
sgidingress=$(aws ec2 describe-security-groups --filters Name=vpc-id,Values=$VPC_ID Name=group-name,Values=ingress.$CLUSTER_NAME --region $AWS_REGION | jq '.["SecurityGroups"][0]["GroupId"]' -r)
sgidnode=$(aws ec2 describe-security-groups --filters Name=vpc-id,Values=$VPC_ID Name=group-name,Values=nodes.$CLUSTER_NAME --region $AWS_REGION | jq '.["SecurityGroups"][0]["GroupId"]' -r)
aws ec2 authorize-security-group-ingress --group-id $sgidingress --protocol tcp --port 443 --cidr 0.0.0.0/0 --region $AWS_REGION
aws ec2 authorize-security-group-ingress --group-id $sgidingress --protocol tcp --port 80 --cidr 0.0.0.0/0 --region $AWS_REGION
aws ec2 authorize-security-group-ingress --group-id $sgidnode --protocol all --port -1 --source-group $sgidingress --region $AWS_REGION
aws ec2 create-tags --region $AWS_REGION --resources $sgidingress --tags Key="Name",Value="ingress.$CLUSTER_NAME" Key="kubernetes.io/cluster/$CLUSTER_NAME",Value="owned" Key="kubernetes:application",Value="kube-ingress-aws-controller"
```

Get ssl certificate arn (should match cluster domain name)
```
aws acm list-certificates
```

Replace region variable on ingress controller yaml definition
```
OSX
sed -i.bak "s/<AWS_REGION>/$AWS_REGION/g"" services/aws-ingress-controller.yml"

Linux
sed -i "s/<AWS_REGION>/$AWS_REGION/g" services/aws-ingress-controller.yml"
```

## Fix dns-controller
```
kubectl edit deployment -nkube-system dns-controller
```
Replace --watch-ingress=false to true and save

## How to add new nodes
```
kops export kubecfg k8s.develop.coincube.io
kops edit ig nodes
```
You will see the yaml file were you can specify the number of nodes, edit for the desired number and save.
Then run 
```
kops update cluster (dry run to plan your changes)
kops update cluster --yes
```
