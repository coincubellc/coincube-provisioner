module "vault_s3_backend" {
  source     = "../../../modules/s3"
  name       = "cc"
  env        = "${var.environment}"
  region     = "${var.region}"
  name_infix = "vault-backend"
}

resource "aws_iam_role" "vault" {
  name = "${lower(var.environment)}-${var.region}-vault"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "vault_s3_backend" {
  name        = "${lower(var.environment)}-${var.region}-vault-s3-bucket"
  description = "Read permissions for vaults s3 bucket"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "${module.vault_s3_backend.arn}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "${module.vault_s3_backend.arn}/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "vault_s3_backend" {
  name       = "vault-policy-attachment"
  roles      = ["${aws_iam_role.vault.name}"]
  policy_arn = "${aws_iam_policy.vault_s3_backend.arn}"
}

resource "aws_iam_instance_profile" "vault" {
  name = "${lower(var.environment)}-${var.region}-vault"
  role = "${aws_iam_role.vault.name}"
}
