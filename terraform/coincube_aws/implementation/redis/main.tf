provider "aws" {
  region = "${var.region}"
}

# Redis for data
module "redis_data" {
  source               = "../../../modules/elasticache"
  environment          = "${var.environment}"
  subnet_ids           = ["${data.terraform_remote_state.vpc.private_subnets}"]
  name_prefix          = "data"
  redis_instance_type  = "${var.redis_instance_type}"
  redis_node_num       = "${var.redis_node_num}"
  security_group_ids   = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "redis_data" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.redis_data_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.redis_data.redis_instance}"]
}

# Redis for brain
module "redis_brain" {
  source               = "../../../modules/elasticache"
  environment          = "${var.environment}"
  subnet_ids           = ["${data.terraform_remote_state.vpc.private_subnets}"]
  name_prefix          = "brain"
  redis_instance_type  = "${var.redis_instance_type}"
  redis_node_num       = "${var.redis_node_num}"
  security_group_ids   = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "redis_brain" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.redis_brain_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.redis_brain.redis_instance}"]
}

# Redis for sentry
module "redis_sentry" {
  count                = "${var.redis_sentry}"
  source               = "../../../modules/elasticache"
  environment          = "${var.environment}"
  subnet_ids           = ["${data.terraform_remote_state.vpc.private_subnets}"]
  name_prefix          = "sentry"
  redis_instance_type  = "${var.redis_instance_type}"
  redis_node_num       = "${var.redis_node_num}"
  security_group_ids   = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "redis_sentry" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.redis_sentry_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.redis_sentry.redis_instance}"]
}

# Redis for coincube-cron-job
module "redis_cron_job" {
  source               = "../../../modules/elasticache"
  environment          = "${var.environment}"
  subnet_ids           = ["${data.terraform_remote_state.vpc.private_subnets}"]
  name_prefix          = "cron-job"
  redis_instance_type  = "${var.redis_instance_type}"
  redis_node_num       = "${var.redis_node_num}"
  security_group_ids   = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "redis_cron_job" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.redis_cron_job_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.redis_cron_job.redis_instance}"]
}


