resource "aws_elasticache_subnet_group" "redis_subnet" {
  count      = "${var.subnet_group != "" ? 0 : 1}"
  name       = "${var.name_prefix}-subnet-group-${var.environment}"
  subnet_ids = ["${var.subnet_ids}"]
}

resource "aws_elasticache_cluster" "redis" {
  count              = "${var.count}"
  cluster_id         = "${var.name_prefix}-redis-${var.environment}"
  engine             = "redis"
  node_type          = "${var.redis_instance_type}"
  port               = "6379"
  num_cache_nodes    = "${var.redis_node_num}"
  subnet_group_name  = "${aws_elasticache_subnet_group.redis_subnet.name}"
  security_group_ids = ["${var.security_group_ids}"]
}
