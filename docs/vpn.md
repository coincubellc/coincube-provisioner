# How to add users to vpn
1. `cd ansible/host_vars/${vpn-host-to-edit}` For example: `cd ansible/host_vars/dev.vpn.coincube.io`
2. Run `ansible-vault encrypt_string` to encrypt the password for the new user, it will ask you for the encryption password and then you have to input the string to encrypt (in this case blabla), after that pres *ctrl+d* two times (don't press return or that character will be encrypted as \n):
```
user@osx:$ ansible-vault encrypt_string
New Vault password:
Confirm New Vault password:
Reading plaintext input from stdin. (ctrl-d to end input)
blabla!vault |
          $ANSIBLE_VAULT;1.1;AES256
          35623735306131386637393366383261653039616261363134356130316236373061323830653463
          3036666362346361316664633336356335393439643266340a343662313530663338303236613561
          65323338333737653436636136653966306362383533373536653462653830626431366335633765
          3533633734323235610a393738323331656366663334366339633231333464613736356138393464
          6137
Encryption successful
```
3. Copy the encrypted string
```
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          35623735306131386637393366383261653039616261363134356130316236373061323830653463
          3036666362346361316664633336356335393439643266340a343662313530663338303236613561
          65323338333737653436636136653966306362383533373536653462653830626431366335633765
          3533633734323235610a393738323331656366663334366339633231333464613736356138393464
          6137
```
4. Edit the file openvpn.yml and add a new line below openvpn_use_pam_users following the yaml syntax for lists
```
- name: newuser
  password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          35623735306131386637393366383261653039616261363134356130316236373061323830653463
          3036666362346361316664633336356335393439643266340a343662313530663338303236613561
          65323338333737653436636136653966306362383533373536653462653830626431366335633765
          3533633734323235610a393738323331656366663334366339633231333464613736356138393464
          6137
```
5. Run ansible playbook to apply changes
```
cd ../../
ansible-playbook playbooks/openvpn.yml --ask-vault-pass --private-key=$INSTANCE_PRIV_KEY
```
***Key stored in Keybase***
Now after execution you can share the new openvpn config file, user and password with the new user                   
