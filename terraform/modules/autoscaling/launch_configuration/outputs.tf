# Launch configuration
output "launch_configuration_id" {
  description = "The ID of the launch configuration"
  value       = "${var.count == 0 ? "" : aws_launch_configuration.this.id}"
}

output "launch_configuration_name" {
  description = "The name of the launch configuration"
  value       = "${var.count == 0 ? "" : aws_launch_configuration.this.name}"
}
