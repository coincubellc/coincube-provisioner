---

- name: Install base packages
  yum:
    name: "{{ item }}"
    state: present
  with_items:
  - ntp
  - vim
  - zsh
  - git
  when: ansible_distribution == 'CentOS'

- name: Install base packages
  apt:
    name: "{{ item }}"
    state: present
  with_items:
  - ntp
  - vim
  - zsh
  - git
  when: ansible_distribution == 'Ubuntu'

- group:
    name: "{{ item }}"
    state: present
  with_items:
  - wheel
  - dockerroot
  when: ansible_distribution == 'Ubuntu'

- name: Setup wheel group with no password
  lineinfile: "dest=/etc/sudoers state=present regexp='^%wheel' line='%wheel ALL=(ALL) NOPASSWD: ALL'"

- name: Remove archived accounts
  user:
    name: "{{ item.name }}"
    state: absent
  with_items:
  - { name: 'ziddey', comment: 'Jibben Nee', uid: '2004', shell: '/usr/bin/zsh', groups: 'wheel' }

- name: Setup users
  user: name={{ item.name }} comment={{ item.comment }} uid={{ item.uid }} shell={{ item.shell }} groups={{ item.groups }}
  with_items:
  - { name: 'bwilson', comment: 'Ben Wilson', uid: '2005', shell: '/bin/bash', groups: 'wheel,dockerroot' }
  - { name: 'ekittell', comment: 'Eric', uid: '2002', shell: '/bin/bash', groups: 'wheel' }
  - { name: 'robert', comment: 'Robert', uid: '2003', shell: '/usr/bin/zsh', groups: 'wheel' }
  - { name: 'gnasr', comment: 'Gabriel Nasr', uid: '2006', shell: '/bin/bash', groups: 'wheel' }
  - { name: 'jenkins', comment: 'Jenkins user', uid: '3001', shell: '/bin/bash', groups: 'dockerroot' }

- name: Setup ssh keys
  authorized_key: user={{ item.user }} key="{{ item.key }}"
  with_items:
  - { user: 'bwilson', key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwdY528mYP73gOBar1h29OmBpAeJrziWRlIZR86vMDuLCZOi6C2M/bm9yeAvdy/8ByZ99GiujdfxsCfZRfkSVZ7SXN1uDempfIzPmQmECb6tmalwxpEMW4LbDNFL/eLR6cW+8iCq8hnVfE/YG7vSu9G0PTbPk575JqyJ4opaqkl6RMK3OrVnvigGaI/ZzWuLwY4Rcre9tlUtVLZfPeUJPfATg1freoGZBH9yH6MtVwI6czFQMkE46+5SVPDdg0NexE2rkQUMN10aLEK0dqg9ro1PLeuICYHCUB472XoTJIwhylk9CaMPhvqoGQEwZPYVxeozb5q+4fZCB41TcJENWp' }
  - { user: 'ekittell', key: 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAsBFECR1lag1GBqzleODwq1Yr/jln2dkT1pRL/kZaPPTm6m0xTSORBT/oH/OensxZjGqMOa8glFy/0dkBiRYbXuI+8OBGewJ1OQZnaki87RP9FyLzf8oPlRQa80G4wn1nllAN8tRdP0ApRF6NIYZYHI/tLQ6vOMKdVnOU13HhFShOHhaLLxwth/NF2YVbuNeHcTkOh7SZDRi2S2IlSJhT8w55aJw50sGeyCy2zA2BkfZBTJuuXrAv9ffTSGjIgfEluTN7B7fJ9m5nYYtcYqbezs6PU5xih4AV80bV75Z3g1YqDAQ9zvbVMX9nlPCoSLUjvD9c9pltgbUo9Qutf/xl6Q== eric@ekittell.com' }
  - { user: 'robert', key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC92wChZnjeKDUQf+wtopGFuXr4Mg1plI8vDZ7QWwR6Mtq5zRqY+4ImgtyEXn0z7G1meZ4CakTf+Yn6ifjoE0lmsBAi7nn3aSeqPkrohzCiCVB80foRKMYWDwsNcH6029qirGdVfcR15XTTg+LQjLnLcfWflMW6BlAIJ2AZWWfstBANLTrlaUWzprsNfT0n5B1Qj3yJ7Mt56pSFQ6DF70zU+6v4W/EEEZISQgJMYOPugXq2gEoFIidKSKrnnfTr4TrAG/aqNF2sV7AIyT6OirCTMtKViDWwxNx3l7qbeSGiTmYptQMZ2zvQxpBrrI5fTNjOo+YlxK795XqgF4dZDjyL1r2uN/Fjlq1M4ccphab7nhZmeS4S6/Lqxbjy6NIRF/eeg68wwiJPem8BHeUcteTMeiuj2MT4Smtjw5y10/6qBGcNnVYjiU/q1g0gdHjZ/vGzDFBPoLlCSmbc3PNdu0qfXIo0LQSaG68iwnSg2YjUaBay7BCD7eAGqBbehuX9GOob+7PQ+D0K8xLAvOSHZQ0MuV0uWS1dC+HIlg3nNxp9olTpTXaabq3ewFkwSe29Zyi9iLkZGCbfth6AzyptSoJco2q97KqW0Nj0ncAn3vK9OFnmpu8nD9DMZ7vbvt6dcZSEz/Wrk9AH0rEY5ixDlR7I9eWds/bP5PUskKHv4+DrJQ== robertwilliamallen@gmail.com' }
  - { user: 'gnasr', key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDFUpdONCs2/eIw3NdSz9ZMKXYGUY3XtQP8wWXU2AatKeXpxaczABLs6bXFFRPDPX9gO8PQwncOI5EP1Y8gcc6YjLHfDZoFYFqAm2SNjecaWoZyZoDMmfO+Pny1x3GdvV13eFrHpz+uf0w5SjAsMmowpy4KgcLzVfVeA7c4dQs4Q33hVsSq0h8yuMCItcB5lZCuuZ0xO+Nxg8192QhFxivyDgnsPoKfltZNy18xHpfuplysfp4XjjqD4OltVN3EzfwJSKgt/kMfkratzmimQpmaY86QxRRtcvXek9YO0lVIKMr8i0/dq5qbfzkjmtYlgT5FOzmmNB52pw470DWOQ3t6L3M0OKECHWDnWEvflsynHvCfF9NUj1kkwmGMY2yHsJ+2+yDrvwWWDQ0hi1z0vdQq+qxiUmmf6PMHYgHnaQamtrvggViUcsbfWMw/PC4hqjYUkcYRyK3TeQfaC/ANFo1doX3xAzdKe3WPMkml9rZR2josxJsFfNMoI3PmTnYK9TW48F7WbMiT1ldrU7kP0cQklHRfkH053bW5Bg3pwHimuhch4stWgmVHp4iv/oSAPHdfpyq6nsfLpmnP5+VUsLpf0IuefDii3xWQboOag6uoDowunc45hsgJvTPxJQUBeN0XRc1K6xNUujYFreIa2qJnG10W7V01TUbdjdWPt7sNVQ== nasr.gab@gmail.com' }
  - { user: 'jenkins', key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDY7Wieg2NQ1vVowxW2qyBNaDVDaseCwdDcyORZSFHbD0oTUmQIqNd4AIy1mmiYstHd30roHDNaUVhnKiBKOBcMaLnJtLMnoOEoop+9LNDah8fS3ReimMB3GTm6ZrllkdIcMqJHLavEDa5yZJ0Qy4GjGuYTo/7anmFd6r488ZNlHNqd+PZwvRHIDlaGv6vB4H9ChAlFjBdY4rFxDHkpFfDSsDkERYe2WJqCsjHtfzCUeDtIGkpghO+LESwQ0X5o2PDeqCvDONKi0O7loFytbe7wNJ3+8oyNn9meGEEfte0n0y4QSSvEmmtImW1X2bKCOaexhCpFer/8FTWyBFxcTVWD' }

- name: Jenkins sudos
  copy:
    src: "{{ item }}"
    dest: /etc/sudoers.d/{{ item }}
  with_items:
    - 11-jenkins

...
