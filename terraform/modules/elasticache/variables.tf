variable "subnet_ids" {
  type        = "list"
  description = "List of private subnet ids"
}

variable "subnet_group" {
  description = "id of the subnet group to use"
  default     = ""
}

variable "environment" {
  description = "Name of the environment"
}

variable "redis_instance_type" {
  description = "Type of instance"
}

variable "redis_node_num" {
  description = "number of nodes in the cluster"
}

variable "security_group_ids" {
  type        = "list"
  description = "List of security groups"
}

variable "name_prefix" {
  description = "Name of the cluster"
}

variable "count" {
  description = "Number of redis instances"
  default     = 1
}