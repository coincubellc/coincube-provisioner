
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| count | Whether to create this resource or not? | string | `1` | no |
| env | Environment | string | - | yes |
| name | The identifier of the resource | string | - | yes |
| subnet_ids | A list of VPC subnet IDs | list | `<list>` | no |

## Outputs

| Name | Description |
|------|-------------|
| db_subnet_group_arn | The ARN of the db subnet group |
| db_subnet_group_id | DB subnet group |

