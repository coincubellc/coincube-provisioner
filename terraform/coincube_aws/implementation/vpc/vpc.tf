module "vpc" {
  source = "../../../modules/vpc"

  name = "${var.vpc_name_prefix}-${var.environment}"
  cidr = "${var.vpc_cidr}"
  azs  = ["${var.vpc_azs}"]

  enable_dns_support = true
  enable_nat_gateway = true
  single_nat_gateway = "${var.single_nat_gateway}"

  private_subnets = ["${var.private_subnets}"]
  public_subnets  = ["${var.public_subnets}"]

  private_subnet_tags = {
    Terraform   = "true"
    Environment = "${var.environment}"
    Name        = "subnet-priv-${var.environment}"
  }

  public_subnet_tags = {
    Terraform   = "true"
    Environment = "${var.environment}"
    Name        = "subnet-pub-${var.environment}"
  }

  database_subnet_tags = {
    Terraform   = "true"
    Environment = "${var.environment}"
    Name        = "subnet-db-${var.environment}"
  }
}
