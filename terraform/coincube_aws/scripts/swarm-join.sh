#!/bin/bash
export MANAGER_IP=${manager_ip};
export MANAGER_PORT=${manager_port};
export TOKEN=${token}
docker swarm join --token $TOKEN $MANAGER_IP:$MANAGER_PORT;