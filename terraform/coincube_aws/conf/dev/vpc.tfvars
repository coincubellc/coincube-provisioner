vpc_name_prefix       = "cc"
vpc_azs               = ["us-east-1a", "us-east-1c"]
vpc_cidr              = "10.0.0.0/16"
private_subnets       = ["10.0.1.0/24", "10.0.2.0/24"]
public_subnets        = ["10.0.11.0/24", "10.0.12.0/24"]
openvpn_instance_type = "t2.small"
key_name              = "coincube-us-east1"