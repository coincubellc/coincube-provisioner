resource "aws_iam_role" "swarm" {
  name = "cc-swarm-${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  depends_on = ["aws_s3_bucket.s3_bucket_swarm"]
}

resource "aws_iam_role_policy" "s3_bucket_swarm" {
  name = "cc-swarm-s3-bucket-${var.environment}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Effect": "Allow",
          "Action": "s3:*",
          "Resource": ["${aws_s3_bucket.s3_bucket_swarm.arn}/*", "arn:aws:s3:::cc-ansible-pull/*"]
      }
  ]
}
EOF

  role = "${aws_iam_role.swarm.id}"
}

resource "aws_iam_role_policy" "coincube_ecr" {
  name = "cc-ecr-${var.environment}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:BatchGetImage"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF

  role = "${aws_iam_role.swarm.id}"
}

resource "aws_iam_instance_profile" "swarm" {
  name = "cc-swarm-${var.environment}"
  role = "${aws_iam_role.swarm.name}"

  depends_on = ["aws_iam_role.swarm"]
}
