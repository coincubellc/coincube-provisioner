provider "aws" {
  region = "${var.region}"
}

resource "aws_s3_bucket" "s3" {
  bucket        = "${var.name}-${lower(var.name_infix)}-${lower(var.env)}-${var.region}"
  force_destroy = true
  region        = "${var.region}"
  acl           = "private"

  versioning {
    enabled = true
  }

  policy = "${var.policy}"

  tags {
    Name        = "${var.name}-${lower(var.name_infix)}-${lower(var.env)}-${var.region}"
    Environment = "${var.env}"
    Creator     = "terraform"
  }

  lifecycle {
    prevent_destroy = false
  }
}
