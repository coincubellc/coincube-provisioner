output "droplet_id" {
  value = ["${digitalocean_droplet.droplet.*.id}"]
}

output "droplet_private_ip" {
  value = ["${digitalocean_droplet.droplet.*.ipv4_address_private}"]
}

output "droplet_ip" {
  value = ["${digitalocean_droplet.droplet.*.ipv4_address}"]
}
