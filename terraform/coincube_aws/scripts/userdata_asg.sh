#!/bin/bash
# Install python to run ansible
sudo apt-get update && sudo apt-get -y install python python-pip;
pip install --upgrade pip;
pip install awscli==1.14.2;
pip install ansible==2.3.2.0;

mkdir -p /etc/ansible && \
touch /etc/ansible/ansible.cfg

tee /etc/ansible/ansible.cfg <<EOF > /dev/null
[defaults]
timeout = 60
inventory = hosts.ini
roles_path = /opt/asg_ansible/ansible/roles
ssh_args = -o ControlMaster=auto -o ControlPersist=600s
remote_tmp = /root/.ansible/tmp
local_tmp = /root/.ansible/tmp
retry_files_enabled = False
deprecation_warnings = False
host_key_checking = False
EOF

aws s3 cp s3://cc-ansible-pull/swarm_github_key.pem /etc/ansible/swarm_github_key.pem && \
chmod 400 /etc/ansible/swarm_github_key.pem;

/usr/local/bin/ansible-pull -U git@github.com:coincubellc/coincube-provisioner.git -o -C master --clean --key-file /etc/ansible/swarm_github_key.pem --accept-host-key -d /opt/asg_ansible/ ansible/playbooks/swarm_instances.yml -i localhost,;

aws s3 cp s3://cc-s3-bucket-swarm-dev/${token}.token /root/${token}.token;

docker swarm join --token `cat /root/${token}.token` ${manager_ip}:2377;