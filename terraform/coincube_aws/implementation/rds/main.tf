provider "aws" {
  region = "${var.region}"
}

resource "aws_db_parameter_group" "default" {
  name        = "cc-pg-mysql-${var.environment}"
  family      = "mysql5.7"
  description = "Parameter group for mysql"

  parameter {
    name  = "range_optimizer_max_mem_size"
    value = "18446744073709551615"
  }
}

module "cc_db_instance" {
  source                 = "../../../modules/database/rds/instance"
  name_prefix            = "cc-mysql-${var.environment}"
  env                    = "${var.environment}"
  multi_az               = "${var.multi_az}"
  allocated_storage      = "${var.allocated_storage}"
  engine                 = "mysql"
  engine_version         = "5.7"
  license_model          = "general-public-license"
  instance_class         = "${var.instance_class}"
  db_name                = "${var.db_name}"
  username               = "${var.username}"
  password               = "${var.password}"
  maintenance_window     = "Sat:07:00-Sat:12:00"
  backup_window          = "13:00-16:00"
  parameter_group_name   = "${aws_db_parameter_group.default.name}"
  db_subnet_group_name   = "${module.db_subnet_group_main.db_subnet_group_id}"
  vpc_security_group_ids = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
}

resource "aws_route53_record" "cc_db_instance" {
  zone_id         = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name            = "${var.db_route53_record}"
  type            = "CNAME"
  ttl             = "300"
  allow_overwrite = false
  records         = ["${module.cc_db_instance.db_instance_address}"]
}
