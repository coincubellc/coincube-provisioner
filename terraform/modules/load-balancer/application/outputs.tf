output "arn" {
  value = "${aws_lb.application.arn}"
}

output "name" {
  value = "${aws_lb.application.name}"
}

output "id" {
  value = "${aws_lb.application.id}"
}

output "dns_name" {
  value = "${aws_lb.application.dns_name}"
}

output "target_group_id" {
  value = "${aws_lb_target_group.tg.id}"
}
