output "redis_brain" {
  value = "${module.redis_brain.redis_instance}"
}

output "redis_brain_fqdn" {
  value = "${aws_route53_record.redis_brain.fqdn}"
}

output "redis_cron_job" {
  value = "${module.redis_cron_job.redis_instance}"
}

output "redis_cron_job_fqdn" {
  value = "${aws_route53_record.redis_cron_job.fqdn}"
}

output "redis_data" {
  value = "${module.redis_data.redis_instance}"
}

output "redis_data_fqdn" {
  value = "${aws_route53_record.redis_data.fqdn}"
}