data "terraform_remote_state" "global" {
  backend = "s3"

  config {
    bucket = "coincube-terraform"
    key    = "global/terraform.tfstate"
    region = "${var.region}"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "openvpn" {
  source          = "../../../modules/aws_ec2_instance"
  name            = "cc-openvpn-${var.environment}"
  environment     = "${var.environment}"
  instance_type   = "${var.openvpn_instance_type}"
  ami             = "${data.aws_ami.ubuntu.image_id}"
  key_name        = "${var.key_name}"
  security_groups = "${aws_security_group.openvpn.id}"
  subnet_id       = "${module.vpc.public_subnets}"
  user_data       = "${file("../../scripts/userdata.sh")}"
}

resource "aws_eip" "openvpn" {
  vpc      = true
  instance = "${element(module.openvpn.id, 0)}"
}

output "openvpn_eip" {
  value = "${aws_eip.openvpn.public_ip}"
}
