resource "aws_lb" "application" {
  name                       = "${var.name}"
  subnets                    = ["${var.subnet_ids}"]
  security_groups            = ["${var.sg_ids}"]
  idle_timeout               = "${var.idle_timeout}"
  internal                   = "${var.internal}"
  load_balancer_type         = "application"
  enable_deletion_protection = "${var.enable_deletion_protection}"

  tags {
    Name        = "${lower(var.name)}"
    Environment = "${var.env}"
    Creator     = "terraform"
  }

  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_lb_target_group" "tg" {
  name        = "${var.name}"
  protocol    = "${var.target_group_protocol}"
  port        = "${var.target_group_port}"
  target_type = "${var.target_type}"
  vpc_id      = "${var.vpc_id}"

  health_check {
    port                = "traffic-port"
    protocol            = "${var.health_check_protocol}"
    path                = "${var.health_check_path}"
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
    timeout             = "${var.health_check_timeout}"
    interval            = "${var.health_check_interval}"
    matcher             = "${var.health_check_success_codes}"
  }
}

resource "aws_lb_listener" "listener" {
  count             = "${length(var.listeners)}"
  load_balancer_arn = "${aws_lb.application.arn}"
  protocol          = "${element(var.listeners[count.index], 0)}"
  port              = "${element(var.listeners[count.index], 1)}"
  ssl_policy        = "${element(var.listeners[count.index], 0) == "HTTPS" ? var.ssl_policy : ""}"
  certificate_arn   = "${element(var.listeners[count.index], 0) == "HTTPS" ? var.certificate_arn : ""}"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg.arn}"
    type             = "forward"
  }
}
