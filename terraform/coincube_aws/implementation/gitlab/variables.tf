variable "bucket" {
  description = "name of the s3 bucket to store the tfstate"
}

variable "region" {}
variable "environment" {}
variable "ec2_key_name" {}
variable "gitlab_route53_record" {}
variable "instance_type" {}
variable "root_block_device_volume_size" {}