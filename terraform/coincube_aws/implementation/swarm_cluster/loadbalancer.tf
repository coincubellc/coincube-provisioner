module "swarm_external_lb" {
  source            = "../../../modules/load-balancer/application"
  name              = "swarm-lb-${var.environment}"
  env               = "${var.environment}"
  vpc_id            = "${data.terraform_remote_state.vpc.vpc_id}"
  subnet_ids        = ["${data.terraform_remote_state.vpc.private_subnets}"]
  sg_ids            = ["${aws_security_group.swarm_external_lb.id}"]
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${var.lb_certificate_arn}"
  internal          = "${var.lb_internal}"
  target_group_port = "80"
  listeners         = {
    "0" = ["HTTPS", "443"]
    "1" = ["HTTP", "80"]
  }
}

resource "aws_route53_record" "swarm_external_lb" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.swarm_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${module.swarm_external_lb.dns_name}"]
}
