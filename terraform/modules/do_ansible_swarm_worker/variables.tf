# DigitalOcean resource variables

variable "image" {
  description = "Name of the OS image"
}

variable "name" {
  description = "Instance name"
}

variable "count" {
  description = "Number of instances"
}

variable "region" {
  description = "Region ID"
}

variable "size" {
  description = "Memory size"
}

variable "private_networking" {
  description = "Bool"
  default     = false
}

variable "ssh_keys" {
  type        = "list"
  description = "A list of SSH IDs or fingerprints to enable in the format [12345, 123456]"
}

variable "user_data" {
  description = "A string of the desired userdata for the droplet"
  default     = ""
}

variable "backups" {
  description = "Bool"
  default     = false
}

variable "volume_ids" {
  description = "A list of the IDs of each block storage volume to be attached to the Droplet."
  type        = "list"
  default     = []
}

# Ansible provisioner needed variables

variable "ssh_user" {
  description = "SSH username"
}

variable "ssh_private_key" {
  description = "Path to the ssh user private key"
}

variable "ansible_playbook" {
  description = "Path to the ansible playbook"
}

variable "token_file" {}
