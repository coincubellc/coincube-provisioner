provider "aws" {
  region = "${var.region}"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

module "vault" {
  source                        = "../../../modules/aws_ec2_instance"
  name                          = "cc-vault"
  environment                   = "${var.environment}"
  instance_type                 = "${var.vault_instance_type}"
  ami                           = "${var.ami}"
  key_name                      = "${var.ec2_key_name}"
  security_groups               = "${data.terraform_remote_state.vpc.sg_internal_traffic}"
  subnet_id                     = "${data.terraform_remote_state.vpc.private_subnets}"
  iam_instance_profile          = "${aws_iam_instance_profile.vault.name}"
  user_data                     = ""
  root_block_device_volume_size = "60"
}

# Create load balancer for vault instances
resource "aws_elb" "vault_lb" {
  name            = "vault-${lower(var.environment)}-${var.region}"
  security_groups = ["${data.terraform_remote_state.vpc.sg_internal_traffic}"]
  subnets         = ["${data.terraform_remote_state.vpc.private_subnets}"]
  internal        = true

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 80
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "${var.cert_arn}"
  }

  listener {
    instance_port      = 8200
    instance_protocol  = "http"
    lb_port            = 8200
    lb_protocol        = "https"
    ssl_certificate_id = "${var.cert_arn}"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8200/v1/sys/health"
    interval            = 30
  }

  instances                   = ["${module.vault.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags {
    Name = "vault-${lower(var.environment)}-${var.region}"
  }
}

resource "aws_route53_record" "vault_lb" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.vault_route53_record}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_elb.vault_lb.dns_name}"]
}

resource "aws_route53_record" "vault_instance" {
  zone_id = "${data.terraform_remote_state.route53.coincube_zone_id}"
  name    = "${var.vault_instance_route53_record}"
  type    = "A"
  ttl     = "300"
  records = ["${module.vault.private_ip}"]
}
