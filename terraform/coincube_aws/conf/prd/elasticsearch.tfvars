instance_type         = "t2.medium.elasticsearch"
elasticsearch_version = "6.2"
es_route53_record     = "es"
kibana_route53_record = "logs"
allow_ips             = "\"10.0.0.0/16\", \"10.1.0.0/16\""
volume_size           = 35
